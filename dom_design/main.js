class GalleryUI {
    constructor(container) {
        this.container = container;
        this.initProperties();
        this.attchEvent();
    }
    initProperties() {
        this.galleryExpand = true;
    }
    /**
     * @private
     */
    attchEvent() {
        this.attachGalleryClickEvent();
        this.attachExpanderClickEvent();
        this.attachMouseMoveEvent();
    }
    /**
    * @private
    */
    attachExpanderClickEvent() {
        var expander = this.getGalleryExpander();
        if (!expander) {
            return;
        }
        let gallery = this.getGallery();
        if (!gallery) {
            return;
        }
        let height = getComputedStyle(gallery).height;
        gallery.style.height = height;
        expander.addEventListener("click", () => {
            var container = this.container;
            if (!container) {
                return;
            }
            if (this.galleryExpand) {
                // collepse
                // this.getContainer().style.removeProperty("height");
                // this.getContainer().style.removeProperty("opacity");
                gallery.classList.add("hide");
                gallery.style.height = `0px`;

                this.updateGalleryExpandStatus();
                if (this._onGalleryCollapse) {
                    this._onGalleryCollapse.call(this);
                }
            } else {
                gallery.style.height = height;
                gallery.classList.remove("hide");;
                this.updateGalleryExpandStatus();
                // this.getContainer().style.opacity = `0`;

                console.debug('expand');
                // expand
                if (this._onGalleryExpander) {
                    this._onGalleryExpander.call(this);
                }
            }
        });
    }
    updateGalleryExpandStatus() {
        this.galleryExpand = !this.galleryExpand;
    }
    /**
    * @private
    */
    attachGalleryClickEvent() {
        var gallery = this.getGallery();
        if (!gallery) {
            return;
        }
        gallery.addEventListener("click", () => {
            if (this._onGalleryClick) {
                this._onGalleryClick.call(this);
            }
        });
    }
    attachMouseMoveEvent() {
        var gallery = this.getGallery();
        if (!gallery) {
            return;
        }
        gallery.addEventListener("mousedown", () => {
            if (this._onGalleryClick) {
                this._onGalleryClick.call(this);
            }
        });
    }
    /**
    * @public
    */
    onGalleryCollapse(callback) {
        this._onGalleryCollapse = callback;
    }
    /**
    * @public
    */
    onGalleryExpand(callback) {
        this._onGalleryExpander = callback;
    }
    /**
    * @public
    */
    onGaleryClick(callback) {
        this._onGalleryClick = callback;
    }
    /**
    * @private
    */
    getGallery() {
        if (!this.container) {
            return;
        }
        return this.container.querySelector(".gallery");
    }
    /**
    * @private
    */
    getGalleryExpander() {
        if (!this.container) {
            return;
        }
        return this.container.querySelector(".gallery-expander");
    }
    /**
   * @private
   */
    getContainer() {
        return this.container;
    }
}

class VideoViewerUI {
    autoHeight() {
    }
    updateContent(style) {
    }
}

window.addEventListener("load", function () {
    var gallery = new GalleryUI(document.querySelector(".gallery-container"));
    var viewer = new VideoViewerUI();
    gallery.onGaleryClick(function () {
        viewer.updateContent();
    });
    gallery.onGalleryCollapse(function () {
        viewer.autoHeight();
    });
    gallery.onGalleryExpand(function () {
        viewer.autoHeight();
    });
});
